---
layout: default
---

<h1 class="text-center">Observatoire de l’accessibilité</h1>

<section class="section text-center">
<ul class="list-unstyled" role="navigation">
{% for website in site %}
  <li class="enlarge-link card-inline card-shadow">
      <h2 class="h3">
        <a class="link-text" href="/audits/{{ website.name | slugify }}/">{{ website.name }}</a>
      </h2>
  </li>
{% endfor %}
</ul>
</section>
