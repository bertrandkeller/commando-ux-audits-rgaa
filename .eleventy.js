const slugify = require("@sindresorhus/slugify");
const fg = require('fast-glob');
const concat = require('concat');
const moment = require('moment');

// ------------------------------------------------------------------------
// Filters
// ------------------------------------------------------------------------

// Filters
module.exports = function (eleventyConfig) {

  let markdownIt = require("markdown-it");
  let markdownItDeflist = require("markdown-it-deflist");
  let markdownItCheckbox = require("markdown-it-checkbox")
  let options = {
    html: true,
    breaks: true,
    linkify: true
  };

  eleventyConfig.setLibrary("md", markdownIt(options)
    .use(markdownItDeflist)
    .use(markdownItCheckbox, {
        divWrap: true,
        divClass: 'checkbox',
    })
  );

  eleventyConfig
    .addPassthroughCopy("assets")

  eleventyConfig.setLiquidOptions({
    dynamicPartials: false,
    strict_filters: true
  });

  eleventyConfig.addFilter('mardownify',(str) => {
    return markdownItRenderer.renderInline(str)
  });

  eleventyConfig.addFilter('jsonify', function (variable) {
    return JSON.stringify(variable);
  });

  eleventyConfig.addFilter("slugify", function (string) {
    return slugify(string, {
      decamelize: false,
      customReplacements: [
        ['%', ' '],
      ]
    });
  });

  eleventyConfig.addFilter('where', function (array, key, value) {
    return array.filter(item => {
      const keys = key.split('.');
      const reducedKey = keys.reduce((object, key) => {
        return object[key];
      }, item);
      return (reducedKey === value ? item : false);
    });
  });

  eleventyConfig.addFilter('sort', function (array, value) {
    return array.sort((a, b) => (a[value] > b[value]) ? 1 : -1)
  });

  // debug utilities
  eleventyConfig.addFilter("dump", obj => util.inspect(obj));

  eleventyConfig.addFilter("dirname", function (filePath) {
    return path.dirname(filePath);
  });

  eleventyConfig.addFilter("push", function (array, element) {
    return array.concat(element).filter(n => n!=='');
  });

  eleventyConfig.addFilter("length", function (element) {
    if (Array.isArray(element)) {
      return element.length;
    }
    if (typeof element === 'object') {
      return Object.keys(element).length;
    }

  });

  eleventyConfig.addFilter("base64", function (url) {
    return Buffer.from(url).toString('base64');
  });

  eleventyConfig.addFilter("dateIso", date => {
    return moment(date).toISOString();
  });

  eleventyConfig.addFilter("date_to_string", function(date) {
    return moment(date).format('DD MMM YYYY');
  });

  eleventyConfig.addFilter("date_to_long_string", function (date) {
    return moment(date).format('DD MMMM YYYY');
  });

  eleventyConfig.addFilter("plus", function (val, number) {
    return Number(val) + Number(number) ;
  });

  eleventyConfig.addFilter("filesizeconvert", function(bytes, decimals = 2) {
      if (bytes === 0) return '0 Bytes';
      const k = 1024;
      const dm = decimals < 0 ? 0 : decimals;
      const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
      const i = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + '<small>' + sizes[i]+ '</small>';
  });

  eleventyConfig.addFilter("commaconvert", function (num) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
      str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 5) {
      str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
  });

  eleventyConfig.addFilter("humanize", function (str) {
    return str
      .replace(/^[\s_]+|[\s_]+$/g, '')
      .replace(/[_\s]+/g, ' ')
      .replace(/^[a-z]/, function (m) { return m.toUpperCase(); });
  });

  eleventyConfig.addFilter("group_by", function (array, value) {
    let group = array.reduce((r, a) => {
      r[a[value]] = [...r[a[value]] || [], a];
      return r;
    }, {});
    return group;
  });

  eleventyConfig.addFilter("isimage", function (path) {
    var entries = fg.sync('assets/**/' + path, '!**/_site');
    if (entries.length > 0) {
      return entries.join("")
    }
  });

  // ------------------------------------------------------------------------
  // Shortcodes
  // ------------------------------------------------------------------------

  eleventyConfig.addShortcode("includeall", function (path) {
    const entries = concat(fg.sync(path));
    return entries
  });

  eleventyConfig.addPairedShortcode("staticfiles", function (content, path) {
    var entries = fg.sync('assets/**/' + path, '!**/_site');
    entries = entries.map(x => '<figure class="flex flex-max"><a href="/' + x + '" class="grid"><img src="/' + x + '" alt="" class="align-items-center"></img></a></figure>')
    if (entries.length > 0) {
      return '<div class="flex flex-column">' + content + entries.join("") + '</div>'
    }
  });

  // ------------------------------------------------------------------------
  // Collections
  // ------------------------------------------------------------------------

  eleventyConfig.addCollection("meetings", function (collection) {
    return collection.getFilteredByGlob("meetings/**/*.md");
  });

  eleventyConfig.addCollection("studies", function (collection) {
    return collection.getFilteredByGlob("studies/**/*.md");
  });

  eleventyConfig.addCollection("feelings", function (collection) {
    return collection.getFilteredByGlob("feelings/**/*.md");
  });

  eleventyConfig.addCollection("actions", function(collection) {
    return collection.getFilteredByGlob("actions/**/*.md");
  });

  // ------------------------------------------------------------------------
  // Plugins
  // ------------------------------------------------------------------------

  //eleventyConfig.addTransform('htmlmin', require('./utils/htmlmin.js'))

  eleventyConfig.addPassthroughCopy("manifest.webmanifest")
  eleventyConfig.addPassthroughCopy("sw-window.mjs")

  eleventyConfig.addWatchTarget("./_assets/css/*.*");


  return {
    dir: {
      layouts: "_layouts",
      output: 'public',
      data: '_data'
    },
    passthroughFileCopy: true,
    templateFormats: ['liquid', 'md', 'html', 'css'],
    htmlTemplateEngine: 'liquid'
  }

}
